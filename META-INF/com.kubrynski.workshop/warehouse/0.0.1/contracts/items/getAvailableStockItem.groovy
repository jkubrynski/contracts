package contracts.stock

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority(20)

    request {
        method(GET())
        url($(consumer(~/\/items\/.*/), producer('/items/available')))
        headers {
            header(accept(), applicationJson())
        }
    }

    response {
        status(200)
        headers {
            header(contentType(), applicationJson())
            header(location(), "${toDslProperty('{{{request.baseUrl}}}')}${fromRequest().url()}")
        }
        body(
                'productId': $(fromRequest().path(1)),
		'bbb' : '{{{request.url}}}',
		'cc': '{{{request.baseUrl}}}/{{{request.url}}}',
		'dd' : '{{{request.path}}}',
                'status': 'AVAILABLE'
        )
    }
}
