package contracts.stock

import org.springframework.cloud.contract.spec.Contract

Contract.make {

    priority(10)

    request {
        method(GET())
        url('/items/reserved')
        headers {
            header(accept(), applicationJson())
        }
    }

    response {
        status(200)
        headers {
            header(contentType(), applicationJson())
        }
        body(
                'productId': 'reserved',
                'status': 'RESERVED'
        )
    }
}
